WITH ["Peter","Michael","Thomas","Mark","Alex","Andreas","Stefan","Daniel","Max","Chris"] AS firstnames,["Maier","Richter","Heiler","Hess","Schmitt","Schneider","Fischer","Weber","Hoffmann","Koch"] AS lastnames, ["Lorem ipsum","Lorem ipsum","dolor sit amet","consetetur sadipscing elitr","sed diam","nonumy eirmod tempor"] AS messages
FOREACH (r IN range(0,9) | 
	CREATE (u:USER {firstname:firstnames[r], lastname:lastnames[r], email:lower(firstnames[r]+"."+lastnames[r] +"@mail.com")})
	FOREACH (r IN range(0,5) | 
		CREATE (m:MESSAGE {content:messages[r]})
		CREATE (u-[:POSTS {date:(round(rand()*751928887703) % 1451928887703)}]->m)
	)
)
WITH "a" as abc
MATCH 	(a:USER {firstname:"Peter"}), (b:USER {firstname:"Michael"}), (c:USER {firstname:"Thomas"}), (d:USER {firstname:"Mark"}), (e:USER {firstname:"Alex"}), 
		(f:USER {firstname:"Andreas"}), (g:USER {firstname:"Stefan"}), (h:USER {firstname:"Daniel"}), (i:USER {firstname:"Max"}), (j:USER {firstname:"Chris"})


MERGE (a)-[:FRIEND_OF]-(b)
MERGE (a)-[:FRIEND_OF]-(c)
MERGE (a)-[:FRIEND_OF]-(d)

MERGE (b)-[:FRIEND_OF]-(e)
MERGE (b)-[:FRIEND_OF]-(f)
MERGE (b)-[:FRIEND_OF]-(g)

MERGE (c)-[:FRIEND_OF]-(h)
MERGE (c)-[:FRIEND_OF]-(i)
MERGE (c)-[:FRIEND_OF]-(j)

MERGE (d)-[:FRIEND_OF]-(a)
MERGE (d)-[:FRIEND_OF]-(b)
MERGE (d)-[:FRIEND_OF]-(c)

MERGE (e)-[:FRIEND_OF]-(d)
MERGE (e)-[:FRIEND_OF]-(f)
MERGE (e)-[:FRIEND_OF]-(g)

MERGE (f)-[:FRIEND_OF]-(e)
MERGE (f)-[:FRIEND_OF]-(h)
MERGE (f)-[:FRIEND_OF]-(i)

MERGE (g)-[:FRIEND_OF]-(j)
MERGE (g)-[:FRIEND_OF]-(a)
MERGE (g)-[:FRIEND_OF]-(b)

MERGE (h)-[:FRIEND_OF]-(c)
MERGE (h)-[:FRIEND_OF]-(d)
MERGE (h)-[:FRIEND_OF]-(e)

MERGE (i)-[:FRIEND_OF]-(f)
MERGE (i)-[:FRIEND_OF]-(g)
MERGE (i)-[:FRIEND_OF]-(h)

MERGE (j)-[:FRIEND_OF]-(i)
MERGE (j)-[:FRIEND_OF]-(a)
MERGE (j)-[:FRIEND_OF]-(b)



WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(a)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mA:MESSAGE) WHERE NOT (a)-[:POSTS]->(mA) WITH rand() as r, mA, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (a)-[:LIKES]->(mA)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(b)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mB:MESSAGE) WHERE NOT (b)-[:POSTS]->(mB) WITH rand() as r, mB, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (b)-[:LIKES]->(mB)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(c)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mC:MESSAGE) WHERE NOT (c)-[:POSTS]->(mC) WITH rand() as r, mC, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (c)-[:LIKES]->(mC)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(d)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mD:MESSAGE) WHERE NOT (d)-[:POSTS]->(mD) WITH rand() as r, mD, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (d)-[:LIKES]->(mD)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(e)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mE:MESSAGE) WHERE NOT (e)-[:POSTS]->(mE) WITH rand() as r, mE, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (e)-[:LIKES]->(mE)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(f)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mF:MESSAGE) WHERE NOT (f)-[:POSTS]->(mF) WITH rand() as r, mF, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (f)-[:LIKES]->(mF)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(g)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mG:MESSAGE) WHERE NOT (g)-[:POSTS]->(mG) WITH rand() as r, mG, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (g)-[:LIKES]->(mG)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(h)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mH:MESSAGE) WHERE NOT (h)-[:POSTS]->(mH) WITH rand() as r, mH, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (h)-[:LIKES]->(mH)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(i)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mI:MESSAGE) WHERE NOT (i)-[:POSTS]->(mI) WITH rand() as r, mI, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (i)-[:LIKES]->(mI)
WITH "a" as abcd, a, b, c, d, e, f, g, h, i, j
MATCH(j)-[:FRIEND_OF]->(:USER)-[:POSTS]->(mJ:MESSAGE) WHERE NOT (j)-[:POSTS]->(mJ) WITH rand() as r, mJ, a, b, c, d, e, f, g, h, i, j ORDER BY r LIMIT 10
MERGE (j)-[:LIKES]->(mJ)