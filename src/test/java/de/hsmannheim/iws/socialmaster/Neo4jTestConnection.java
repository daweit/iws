package de.hsmannheim.iws.socialmaster;

import java.sql.SQLException;
import java.util.Properties;

import org.neo4j.jdbc.Driver;
import org.neo4j.jdbc.Neo4jConnection;

public class Neo4jTestConnection extends Neo4jConnection {

    public Neo4jTestConnection(Driver driver, String jdbcUrl, Properties properties) throws SQLException {
        super(driver, jdbcUrl, properties);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void close() throws SQLException {
        // do nothing
    }

}
