package de.hsmannheim.iws.socialmaster;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.neo4j.jdbc.Neo4jConnection;
import org.neo4j.jdbc.Neo4jStatement;

import de.hsmannheim.iws.socialmaster.control.dao.MessageDao;
import de.hsmannheim.iws.socialmaster.control.dao.UserDao;
import de.hsmannheim.iws.socialmaster.model.Message;
import de.hsmannheim.iws.socialmaster.model.User;

public class MessageDaoTest {

    private Datasource datasource;
    private MessageDao messageDao;
    private UserDao userDao;

    @Before
    public void setup() throws ClassNotFoundException, SQLException {

        datasource = new Datasource("mem");
        datasource.setup();

        Neo4jConnection con = datasource.getConnection();
        Neo4jTestConnection tcon = new Neo4jTestConnection(con.getDriver(), "jdbc:neo4j:mem", con.getProperties());
        tcon.setAutoCommit(false);

        datasource.setConnection(tcon);

        messageDao = new MessageDao();
        messageDao.setDatasource(datasource);
        messageDao.init();

        userDao = new UserDao();
        userDao.setDatasource(datasource);
        userDao.init();

        loadTestData();
    }

    @Test
    public void test() throws SQLException {
        Assert.assertEquals(10, userDao.findAllUsers().size());
    }

    @Test
    public void testPostMessage() {
        try {
            Optional<User> user = userDao.findAllUsers().stream().findFirst();
            Optional<Message> message = Optional.empty();
            Long databaseMessageId = 0L;
            if (user.isPresent()) {
                message = messageDao.postMessage(user.get(), new Message("Test Message"));
                databaseMessageId = message.get().getNodeId();
            }
            Assert.assertEquals(messageDao.findMessageById(databaseMessageId).get().getContent(), message.get().getContent());
        } catch (SQLException e) {
            Assert.fail();
        }
    }


    private void loadTestData() {
        try {
            StringBuilder sb = new StringBuilder();
            InputStream is = MessageDaoTest.class.getResourceAsStream("/createDevelopmentData.cypher");
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            Neo4jConnection connection = datasource.getConnection();
            String s = sb.toString();

            try (Neo4jStatement statement = new Neo4jStatement(connection)) {
                statement.executeQuery(s);
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            Assert.fail();
        }

    }

}