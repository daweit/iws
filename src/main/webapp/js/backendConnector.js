﻿var backendConnector = new function () {

    //	var currentUser = {
    //	    "firstname": "a",//"Timo",
    //	    "lastname": "b",//"Notheisen",
    //	    "email": "a.b@c.d"//"timo.notheisen@gmail.com"
    //	};

    this.login = function (form) {
        var found = false;
        this.getAllUsers(function (allUsers) {
            allUsers.forEach(function (user) {
                if (user.email === form.email.value) {
                    found = true;
                    $.cookie("currentUser", JSON.stringify(user), {
                	   path: '/'        
                	});
                }
            });
            if (found) {
                window.open("html/Messages.html", "_self");
            }
            else {
                alert("Error Password or email"); 
            }
        });
    }
    
    this.logout = function() {
    	$.removeCookie('currentUser', { path: '/' });
    	//$.cookie("currentUser", null, { path: '/' });
    }

    this.getFriends = function (callback) {
        var self = this;
        $.ajax({
            url: this.buildUrl("users/friends"),
            type: "GET",
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });
    };

    this.getAllUsers = function (callback) {

        $.ajax({
            url: this.buildUrl("users"),
            type: "GET",
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        });



        /*return [
	        {
	            "userid": 1,
	            "firstname": "Daniel",
	            "lastname": "Weidle",
	            "email": "daniel.weidle@ImABadLittleGirl.com"
	        },
	        {
	            "userid": 2,
	            "firstname": "Alex",
	            "lastname": "Schramm",
	            "email": "alex.schramm@ProbedByAliens.com"
	        },
	        {
	            "userid": 3,
	            "firstname": "Jochen",
	            "lastname": "Schwander",
	            "email": "jochen.schwander@ImaTerrorist.com"
	        }
	    ];*/
    };

    this.createUser = function (email, firstname, lastname, password, callback) {

        var data = {
            "email": email,
            "firstname": firstname,
            "lastname": lastname/*,
				"password": password*/
        }

        $.ajax({
            url: this.buildUrl("users"),
            type: "POST",
            data: JSON.stringify(data),
            success: function (result) {
                if(callback !== null) if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        });
    }

    this.getMessageHistory = function (callback) {
        var self = this;
        $.ajax({
            url: this.buildUrl("messages"),
            type: "GET",
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json;charset=utf-8",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });
    };
    
    this.getAllHashtags = function (callback) {
        var self = this;
        
        $.ajax({
            url: this.buildUrl("hashtags"),
            type: "GET",
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json;charset=utf-8",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });
    }
    
    this.getMessagesWithHashtag = function (hashtag, callback) {
        var data = {
            "hashtags": hashtag
        };
        
        var self = this;
        $.ajax({
            url: this.buildUrl("messages"),
            type: "GET",
            data: JSON.stringify(data),
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json;charset=utf-8",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });
    }

    this.sendMessage = function (message, callback) {

        var data = {
            "content": message
        };

        var self = this;
        $.ajax({
            url: this.buildUrl("messages"),
            type: "POST",
            data: JSON.stringify(data),
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json;charset=utf-8",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });

    };

    this.getRelationshipsToSuggestedFriends = function (callback) {
        // TODO
        throw "getRelationshipsToSuggestedFriends is not yet implemented";
    }

    this.getSuggestedFriends = function (callback) {
        var self = this;
        $.ajax({
            url: this.buildUrl("users/friendsuggestions"),
            type: "GET",
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });

    };
    this.removeFriend = function (email, callback) {
        var data = {
            "email": email
        };
        var self = this;
        $.ajax({
            url: this.buildUrl("users/friend"),
            type: "DELETE",
            data: JSON.stringify(data),
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });
    };

    this.addFriend = function (email, callback) {
        var data = {
            "email": email
        };

        var self = this;
        $.ajax({
            url: this.buildUrl("users/friend"),
            type: "POST",
            data: JSON.stringify(data),
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });
    };

    this.getMessagesThatFriendsLike = function (callback) {
        var self = this;
        $.ajax({
            url: this.buildUrl("messages/like"), 
            type: "GET",
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json;charset=utf-8",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });
    }

    this.likeMessage = function (messageId, like, callback) {

        var data = {
            "id": messageId
        };
    	
        var self = this;
        $.ajax({
            url: this.buildUrl("messages/like"),
            type: like ? "POST" : "DELETE",
            data: JSON.stringify(data),
            success: function (result) {
                if(callback !== null) callback(result);
            },
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Basic " + btoa(self.getCurrentUser().email + ":" + "doesntmatter"));
            }
        });

    }


    this.buildUrl = function (route) {
        return "/mysocialnetwork/v1/" + route;
    }

    this.getCurrentUser = function () {
    	return JSON.parse($.cookie("currentUser"));
        //return JSON.parse(localStorage.getItem("currentUser"));
    }
}