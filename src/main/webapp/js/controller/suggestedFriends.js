﻿angular.module("nosqlapp").controller("suggestFriendsCtrl", function ($scope) {
    setActivePage("suggestedFriends");

    $scope.currentUser = backendConnector.getCurrentUser();
    backendConnector.getSuggestedFriends(function(suggestedFriends) {
    	$scope.suggestedFriends = suggestedFriends;
    	$scope.$apply();
    });

    $scope.addFriend = function (index) {
    	backendConnector.addFriend($scope.suggestedFriends[index].email, function(result) {
    		$scope.addedUser = $scope.suggestedFriends[index];
            $scope.suggestedFriends.splice(index, 1);
        	$scope.$apply();
        	$('#friendAddedAlert').show();
    	});
    }
	
	$scope.showRelationships = function(index) {
		$("[data-toggle=popover_" + index + "]").popover();
	}
})