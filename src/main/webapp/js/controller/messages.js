angular.module("nosqlapp").controller("messagesCtrl", function ($scope) {
    setActivePage("messages");

    $scope.newMessage = "";
    $scope.currentUser = backendConnector.getCurrentUser();
    
    backendConnector.getMessageHistory(function(messageHistory) {
    	initialize(messageHistory);
    });
    
    backendConnector.getAllHashtags(function(hashtags) {
    	$scope.allHashtags = hashtags;
    });
    
    $scope.send = function () {
    	if($scope.newMessage === "") {
    		return;
    	}
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        var dateString = dd + "." + mm + "." + yyyy;

        backendConnector.sendMessage($scope.newMessage, function(message) {
        	message.date = new Date(message.date);
            $scope.messages.unshift(message);
        	$scope.$apply();
        });
        $scope.newMessage = "";
    }

    $scope.getClass = function (message) {
        if (message.sender.email === $scope.currentUser.email) {
            return "myMessage";
        }
        else {
            return "otherMessage";
        }
    }
    
    $scope.filterForHashtag = function(hashtag) {
    	backendConnector.getMessagesWithHashtag(hashtag, function(messages) {
    		initialize(messages);
    	});
    }


    $scope.orderByDate = function (item) {
        return item.date;
    };

    $scope.likeMessage = function (messageId, like) {
    	backendConnector.likeMessage(messageId, like, function(result) {});
    }
    
    var initialize = function(messages) {
        $scope.messages = messages;
        
        $scope.messages.forEach(function(message) {
        	message.date = new Date(message.date);
        });
        
    	$scope.$apply();
    };
})