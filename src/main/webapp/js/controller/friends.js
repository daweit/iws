angular.module("nosqlapp").controller("friendsCtrl", function ($scope) {

    setActivePage("friends");

    $scope.currentUser = backendConnector.getCurrentUser();
    backendConnector.getFriends(function(friends) {
    	$scope.friends = friends;
    	$scope.$apply();
    });

    $scope.removeFriend = function (index) {
    	backendConnector.removeFriend($scope.friends[index].email, function() {
            $scope.removedUser = $scope.friends[index];
            $scope.friends.splice(index, 1);
            $scope.$apply();
        	$('#friendRemovedAlert').show();
    	});
    }
})