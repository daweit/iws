﻿angular.module("nosqlapp").controller("searchFriendsCtrl", function ($scope) {
    setActivePage("searchFriends");

    $scope.currentUser = backendConnector.getCurrentUser();
    backendConnector.getAllUsers(function(allUsers) {
    	
    	backendConnector.getFriends(function(friends) {
    		
        	$scope.users = [];
        	
        	allUsers.forEach(function(user) {
        		
        		// exclude users that are already friends
        		var userIsAlreadyFriend = false;
        		friends.forEach(function(friend) {
        			if(friend.email === user.email) {
        				userIsAlreadyFriend = true;
        			}
        		});
        		
        		if(!userIsAlreadyFriend && user.email !== $scope.currentUser.email) {
            		$scope.users.push(user);
        		}
        	});
        	$scope.$apply();
        	
    	});
    	
    });

    $scope.addFriend = function (index) {
    	backendConnector.addFriend($scope.users[index].email, function(addedFriend) {
    		$scope.addedUser = $scope.users[index];
            $scope.users.splice(index, 1);
        	$scope.$apply();
        	$('#friendAddedAlert').show();
        });
    }
})