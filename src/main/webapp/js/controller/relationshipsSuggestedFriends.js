﻿angular.module("nosqlapp").controller("relationshipsToSuggestedFriendsCtrl", function ($scope) {
    setActivePage("relationshipsSuggestedFriends");

    $scope.currentUser = backendConnector.getCurrentUser();
    $scope.relationships = backendConnector.getRelationshipsToSuggestedFriends(function(relationships) {
    	
    });

    $scope.addFriend = function (index) {
    	backendConnector.addFriend($scope.friends[index].userid, function(result) {
            $scope.friends.splice(index, 1);
    	});
    }
})