﻿angular.module("nosqlapp").controller("likesOfFriendsCtrl", function ($scope) {
    
    $scope.currentUser = backendConnector.getCurrentUser();
    
    backendConnector.getMessagesThatFriendsLike(function(messages) {
    	initialize(messages);
    });
    
    backendConnector.getAllHashtags(function(hashtags) {
    	$scope.allHashtags = hashtags;
    });

    $scope.getClass = function (message) {
        if (message.sender.email === $scope.currentUser.email) {
            return "myMessage";
        }
        else {
            return "otherMessage";
        }
    };
    
    $scope.filterForHashtag = function(hashtag) {
    	backendConnector.getMessagesWithHashtag(hashtag, function(messages) {
    		initialize(messages);
    	});
    };


    $scope.orderByDate = function (item) {
        return item.date;
    };

    $scope.likeMessage = function (messageId, like) {
    	backendConnector.likeMessage(messageId, like, function(result) {});
    };
    
    var initialize = function(messages) {
        $scope.messages = messages;
        
        $scope.messages.forEach(function(message) {
        	message.date = new Date(message.date);
        });
        
    	$scope.$apply();
    };
})