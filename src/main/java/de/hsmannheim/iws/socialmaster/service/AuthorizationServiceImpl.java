package de.hsmannheim.iws.socialmaster.service;

import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.core.HttpHeaders;

import de.hsmannheim.iws.socialmaster.control.dao.UserDao;
import de.hsmannheim.iws.socialmaster.model.User;

@Stateless
public class AuthorizationServiceImpl implements AuthorizationService {

    public final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @EJB
    private UserDao userDao;

    /**
     * Dummy impl.
     * 
     * Erwartet wird eine base64 encodierte emailadresse
     */
    @Override
    public User auth(HttpHeaders headers) {

        String authHeader = headers.getHeaderString("Authorization");
        String base64Credentials = authHeader.substring("Basic".length()).trim();
        String[] credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8")).split(":");

        Optional<User> optUser;

        try {
            if (credentials.length == 2) {
                optUser = userDao.findUserByEmail(credentials[0]);
                if (optUser.isPresent()) {
                    LOGGER.log(Level.INFO, "Logged in user with {0}", new Object[] { optUser.get() });
                    return optUser.get();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
