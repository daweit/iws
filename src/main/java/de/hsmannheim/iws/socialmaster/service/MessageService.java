package de.hsmannheim.iws.socialmaster.service;

import java.util.List;

import javax.ejb.Local;

import de.hsmannheim.iws.socialmaster.model.Message;
import de.hsmannheim.iws.socialmaster.model.User;

@Local
public interface MessageService {

    public Message postMessage(User user, Message message);

    public Message likeMessage(User user, Message message);

    public Message unlikeMessage(User user, Message message);

    public List<Message> getFriendsMessages(User user);

}
