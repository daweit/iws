package de.hsmannheim.iws.socialmaster.service;

import javax.ejb.Local;
import javax.ws.rs.core.HttpHeaders;

import de.hsmannheim.iws.socialmaster.model.User;

@Local
public interface AuthorizationService {

    public User auth(HttpHeaders headers);

}
