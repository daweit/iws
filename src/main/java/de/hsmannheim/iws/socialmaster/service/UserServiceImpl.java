package de.hsmannheim.iws.socialmaster.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import de.hsmannheim.iws.socialmaster.control.dao.RelationDao;
import de.hsmannheim.iws.socialmaster.control.dao.UserDao;
import de.hsmannheim.iws.socialmaster.model.RelType;
import de.hsmannheim.iws.socialmaster.model.User;

@Stateless
public class UserServiceImpl implements UserService {

    @EJB
    private UserDao userDao;

    @EJB
    private RelationDao relationDao;

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        try {
            users = userDao.findAllUsers();
        } catch (SQLException ignore) {
        }
        return users;
    }

    public User createUser(User user) {
        User createdUser = null;
        try {
            Optional<User> optUser = userDao.createUser(user);
            if (optUser.isPresent()) {
                createdUser = optUser.get();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return createdUser;
    }

    @Override
    public RelType addFriend(User user, User friend) {
        Optional<User> friendOptional;

        try {
            friendOptional = userDao.findUserByEmail(friend.getEmail());

            if (friendOptional.isPresent()) {
                relationDao.createRelation(user, friendOptional.get(), RelType.FRIEND_OF);
                return RelType.FRIEND_OF;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public RelType removeFriend(User user, User friend) {
        Optional<User> friendOptional;

        try {
            friendOptional = userDao.findUserByEmail(friend.getEmail());

            if (friendOptional.isPresent()) {
                relationDao.deleteRelation(user, friendOptional.get(), RelType.FRIEND_OF);
                return RelType.FRIEND_OF;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Returns all suggested Friends
     */
    @Override
    public List<User> findFriendsSuggestions(User currentUser) {
        List<User> friends = null;
        if (currentUser != null) {
            try {
                friends = userDao.findAllSuggestedFriends(currentUser);
            } catch (SQLException ignore) {
            }
        }
        return friends;
    }

    @Override
    public List<User> getFriends(User currentUser) {
        List<User> friends = null;
        
        if(currentUser != null) {
            try {
                friends = userDao.findFriends(currentUser);
            } catch (SQLException ignore) {
            }
        }
        return friends;
    }

}
