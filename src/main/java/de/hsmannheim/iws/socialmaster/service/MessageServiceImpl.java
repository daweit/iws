package de.hsmannheim.iws.socialmaster.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import de.hsmannheim.iws.socialmaster.control.dao.MessageDao;
import de.hsmannheim.iws.socialmaster.control.dao.RelationDao;
import de.hsmannheim.iws.socialmaster.model.Message;
import de.hsmannheim.iws.socialmaster.model.RelType;
import de.hsmannheim.iws.socialmaster.model.User;

@Stateless
public class MessageServiceImpl implements MessageService {

    @EJB
    private MessageDao messageDao;

    @EJB
    private RelationDao relationDao;

    @Override
    public Message postMessage(User user, Message message) {
        Optional<Message> optMessage;
        Message output = null;
        
        try {
            optMessage = messageDao.postMessage(user, message);
            if (optMessage.isPresent()) {
                output = optMessage.get();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return output;
    }

    public Message likeMessage(User user, Message message) {
        Optional<Message> optMessage;
        try {
            optMessage = messageDao.findMessageById(message.getId());
            if (optMessage.isPresent()) {
                relationDao.createRelation(user, optMessage.get(), RelType.LIKES);
                return message;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Message unlikeMessage(User user, Message message) {
        Optional<Message> optMessage;
        try {
            optMessage = messageDao.findMessageById(message.getId());
            if (optMessage.isPresent()) {
                relationDao.deleteRelation(user, optMessage.get(), RelType.LIKES);
                return message;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Message> getFriendsMessages(User user) {
        List<Message> messages = null;
        try {
        messages = messageDao.findFriendsMessages(user);
        } catch (SQLException ignore) {
        }
        return messages;
    }

}
