package de.hsmannheim.iws.socialmaster.service;

import java.util.List;

import javax.ejb.Local;

import de.hsmannheim.iws.socialmaster.model.RelType;
import de.hsmannheim.iws.socialmaster.model.User;

@Local
public interface UserService {

    public List<User> getAllUsers();

    public User createUser(User user);

    public RelType removeFriend(User user, User friend);

    public RelType addFriend(User user, User friend);

    public List<User> findFriendsSuggestions(User currentUser);

    public List<User> getFriends(User currentUser);

}
