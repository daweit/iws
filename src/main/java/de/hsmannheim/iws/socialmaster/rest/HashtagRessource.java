package de.hsmannheim.iws.socialmaster.rest;

import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.hsmannheim.iws.socialmaster.model.User;
import de.hsmannheim.iws.socialmaster.service.AuthorizationService;

@Path("/hashtags")
public class HashtagRessource {

    @EJB
    private AuthorizationService authService;

    @Context
    private HttpHeaders headers;

    /**
     * Dummy Implementierung für Zusatzaufgabe
     */
    @GET
    public Response getHashtags() {
        Response response;

        User currentUser = authService.auth(headers);
        List<String> hashtags = Arrays.asList(new String[] { "iws", "cool", "zufrühfertig" });

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if (hashtags != null) {
            response = Response.status(Status.OK).entity(hashtags).build();
        } else {
            response = Response.status(Status.BAD_REQUEST).build();
        }
        return response;
    }

}
