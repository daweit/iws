package de.hsmannheim.iws.socialmaster.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.hsmannheim.iws.socialmaster.model.RelType;
import de.hsmannheim.iws.socialmaster.model.User;
import de.hsmannheim.iws.socialmaster.service.AuthorizationService;
import de.hsmannheim.iws.socialmaster.service.UserService;

@Path("/users")
public class UserRessource {

    @EJB
    private UserService userService;

    @EJB
    private AuthorizationService authService;

    @Context
    private HttpHeaders headers;

    @GET
    public Response getUsers() {
        Response response;
        List<User> usersList = userService.getAllUsers();
        response = Response.status(Status.OK).entity(usersList).build();
        return response;
    }

    @POST
    public Response createUser(User user) {
        Response response;
        User createdUser;

        if ((createdUser = userService.createUser(user)) != null) {
            response = Response.status(Status.OK).entity(createdUser).build();
        } else {
            response = Response.status(Status.BAD_REQUEST).build();
        }
        return response;
    }

    @POST
    @Path("/friend")
    public Response addFriend(User friend) {
        Response response;
        RelType relation;

        User currentUser = authService.auth(headers);

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if ((relation = userService.addFriend(currentUser, friend)) == null) {
            response = Response.status(Status.BAD_REQUEST).build();
        } else {
            response = Response.status(Status.OK).build();
        }
        return response;
    }

    @DELETE
    @Path("/friend")
    public Response removeFriend(User friend) {
        Response response;
        RelType relation;

        User currentUser = authService.auth(headers);

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if ((relation = userService.removeFriend(currentUser, friend)) == null) {
            response = Response.status(Status.BAD_REQUEST).build();
        } else {
            response = Response.status(Status.OK).build();
        }
        return response;
    }

    @GET
    @Path("/friends")
    public Response getFriends() {

        Response response;
        List<User> friends;

        User currentUser = authService.auth(headers);

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if ((friends = userService.getFriends(currentUser)) == null) {
            response = Response.status(Status.BAD_REQUEST).build();
        } else {
            response = Response.status(Status.OK).entity(friends).build();
        }
        return response;
    }

    @GET
    @Path("/friendsuggestions")
    public Response getFriendsSuggestions() {
        Response response;

        List<User> friendsSuggestions;
        User currentUser = authService.auth(headers);

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if ((friendsSuggestions = userService.findFriendsSuggestions(currentUser)) != null) {
            response = Response.status(Status.OK).entity(friendsSuggestions).build();
        } else {
            response = Response.status(Status.BAD_REQUEST).build();
        }
        return response;

    }

}
