package de.hsmannheim.iws.socialmaster.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.hsmannheim.iws.socialmaster.model.Message;
import de.hsmannheim.iws.socialmaster.model.User;
import de.hsmannheim.iws.socialmaster.service.AuthorizationService;
import de.hsmannheim.iws.socialmaster.service.MessageService;

@Path("/messages")
public class MessageRessource {

    @EJB
    private MessageService messageService;

    @EJB
    private AuthorizationService authService;

    @Context
    private HttpHeaders headers;

    @POST
    public Response postMessage(Message message) {
        Response response;

        User currentUser = authService.auth(headers);
        Message postedMessage;

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if (message != null && (postedMessage = messageService.postMessage(currentUser, message)) != null) {
            response = Response.status(Status.OK).entity(postedMessage).build();
        } else {
            response = Response.status(Status.BAD_REQUEST).build();
        }
        return response;
    }

    @POST
    @Path("/like")
    public Response likeMessage(Message message) {
        Response response;

        User currentUser = authService.auth(headers);

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if (message != null && messageService.likeMessage(currentUser, message) != null) {
            response = Response.status(Status.OK).build();
        } else {
            response = Response.status(Status.BAD_REQUEST).build();
        }
        return response;
    }

    @DELETE
    @Path("/like")
    public Response unlikeMessage(Message message) {
        Response response;

        User currentUser = authService.auth(headers);

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if (message != null && messageService.unlikeMessage(currentUser, message) != null) {
            response = Response.status(Status.OK).build();
        } else {
            response = Response.status(Status.BAD_REQUEST).build();
        }
        return response;
    }

    /**
     * Dummy Impl für Zusatzaufgabe
     */
    @GET
    @Path("/like")
    public Response getMessagesLikedByFriends() {
        Response response;

        User currentUser = authService.auth(headers);
        List<Message> messages;

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if ((messages = messageService.getFriendsMessages(currentUser)) != null) { // getFriendsMessages
                                                                                          // muss
                                                                                          // durch
                                                                                          // die
                                                                                          // ensprechende
                                                                                          // Impl
                                                                                          // ersetzt
                                                                                          // werden
            response = Response.status(Status.OK).entity(messages).build();
        } else {
            response = Response.status(Status.BAD_REQUEST).build();
        }
        return response;
    }

    /**
     * Implementierung für List als QueryParam
     * (https://tools.ietf.org/html/rfc3986) wird für die Zusatzaufgabe benötigt
     * - kann für Standardaufgabe auser acht gelassen werden
     * 
     */
    @GET
    public Response getMessages(@QueryParam("hashtags") final List<String> hashtags) {
        Response response;
        List<Message> messages;

        User currentUser = authService.auth(headers);

        if (currentUser == null) {
            response = Response.status(Status.UNAUTHORIZED).build();
        } else if ((messages = messageService.getFriendsMessages(currentUser)) != null) {
            response = Response.status(Status.OK).entity(messages).build();
        } else {
            response = Response.status(Status.BAD_REQUEST).build();
        }
        return response;
    }

}
