package de.hsmannheim.iws.socialmaster.control.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.neo4j.jdbc.Neo4jConnection;
import org.neo4j.jdbc.Neo4jPreparedStatement;

import de.hsmannheim.iws.socialmaster.Datasource;
import de.hsmannheim.iws.socialmaster.model.User;

@Stateless
public class UserDao {

    @EJB
    private Datasource datasource;

    private Neo4jConnection connection;

    @PostConstruct
    public void init() {
        connection = datasource.getConnection();
    }

    public Optional<User> createUser(User user) throws SQLException {
        Optional<User> userOptional;
        try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, User.CREATE_QUERY)) {
            Map<String, Object> userProperties = user.toMap();
            statement.setObject(1, userProperties);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                userOptional = Optional.of(new User(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            } else {
                userOptional = Optional.empty();
            }

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            if (!connection.isClosed()) {
                connection.rollback();
            }
            userOptional = Optional.empty();
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        return userOptional;
    }

    public List<User> findAllUsers() throws SQLException {
        List<User> usersList = new ArrayList<>();

        try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, User.FIND_ALL)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                usersList.add(new User(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            connection.commit();
        } catch (SQLException ignore) {
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        return usersList;
    }

    public Optional<User> findUserByEmail(String email) throws SQLException {
        Optional<User> userOptional;
        try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, User.FIND_BY_MAIL)) {
            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                userOptional = Optional.of(new User(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            } else {
                userOptional = Optional.empty();
            }
            connection.commit();
        } catch (SQLException e) {
            userOptional = Optional.empty();
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        return userOptional;

    }

    public List<User> findAllSuggestedFriends(User user) throws SQLException {
        List<User> usersList = new ArrayList<>();
        try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, User.GET_ALL_FRIENDS_SUGGESTIONS)) {
            statement.setLong(1, user.getNodeId());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                usersList.add(new User(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            connection.commit();
        } catch (SQLException ignore) {
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        return usersList;

    }

    public List<User> findFriends(User currentUser) throws SQLException {
        List<User> usersList = new ArrayList<>();
        try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, User.GET_FRIENDS)) {
            statement.setLong(1, currentUser.getNodeId());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                usersList.add(new User(rs.getLong(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            connection.commit();
        } catch (SQLException ignore) {
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        return usersList;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

}
