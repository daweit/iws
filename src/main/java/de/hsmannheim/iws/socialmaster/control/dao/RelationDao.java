package de.hsmannheim.iws.socialmaster.control.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.neo4j.jdbc.Neo4jConnection;
import org.neo4j.jdbc.Neo4jPreparedStatement;

import de.hsmannheim.iws.socialmaster.Datasource;
import de.hsmannheim.iws.socialmaster.model.IwsEntity;
import de.hsmannheim.iws.socialmaster.model.RelType;
import de.hsmannheim.iws.socialmaster.model.Relation;

@Stateless
public class RelationDao {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    @EJB
    private Datasource datasource;

    private Neo4jConnection connection;

    @PostConstruct
    public void init() {
        connection = datasource.getConnection();
    }

    public Relation createRelation(IwsEntity from, IwsEntity to, RelType relation) {
        String query = "";
        Relation rel = null;

        switch (relation) {
        case FRIEND_OF:
            query = RelType.CREATE_FRIEND_OF_QUERY;
            rel = new Relation(RelType.FRIEND_OF);
            break;
        case LIKES:
            query = RelType.CREATE_LIKES_QUERY;
            rel = new Relation(RelType.LIKES);
            break;
        default:
            return rel;
        }
        try {
            try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, query)) {
                statement.setLong(1, from.getNodeId());
                statement.setLong(2, to.getNodeId());

                ResultSet rs = statement.executeQuery();

                if (rs.next()) {
                    rel.setCreation(new Date(rs.getLong(1)));
                }
                connection.commit();
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, "Relation already exists - error due UNIQUE constraint");
            } finally {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "Unable to close connection or rollback");
        }
        return rel;
    }

    public void deleteRelation(IwsEntity from, IwsEntity to, RelType relation) throws SQLException {
        String query = "";

        switch (relation) {
        case FRIEND_OF:
            query = RelType.DELETE_FRIEND_OF_QUERY;
            break;
        case LIKES:
            query = RelType.DELETE_LIKES_QUERY;
            break;
        default:
            return;
        }

        try {
            try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, query)) {
                statement.setLong(1, from.getNodeId());
                statement.setLong(2, to.getNodeId());

                ResultSet rs = statement.executeQuery();
                connection.commit();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, "Relation doesn't exist");
            } finally {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.WARNING, "Unable to close connection or rollback");
        }
    }

}
