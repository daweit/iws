package de.hsmannheim.iws.socialmaster.control.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.neo4j.jdbc.Neo4jConnection;
import org.neo4j.jdbc.Neo4jPreparedStatement;

import de.hsmannheim.iws.socialmaster.Datasource;
import de.hsmannheim.iws.socialmaster.model.Message;
import de.hsmannheim.iws.socialmaster.model.User;

@Stateless
public class MessageDao {

    @EJB
    private Datasource datasource;

    private Neo4jConnection connection;

    @PostConstruct
    public void init() {
        connection = datasource.getConnection();
    }

    public Optional<Message> postMessage(User user, Message message) throws SQLException {
        Optional<Message> messageOptional;
        try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, Message.CREATE_QUERY)) {
            statement.setLong(1, user.getNodeId());
            Map<String, Object> messageProperties = message.toMap();
            statement.setObject(2, messageProperties);
            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                messageOptional = Optional.of(
                        new Message(rs.getLong(1), rs.getString(2), new User(rs.getLong(4), rs.getString(5), rs.getString(6), rs.getString(7)), new Date(rs.getLong(3)), false));
            } else {
                messageOptional = Optional.empty();
            }

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            messageOptional = Optional.empty();
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        return messageOptional;
    }

    public Optional<Message> findMessageById(Long id) throws SQLException {
        Optional<Message> messageOptional;

        try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, Message.FIND_BY_ID_QUERY)) {
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                messageOptional = Optional.of(new Message(rs.getLong(1), rs.getString(2)));
            } else {
                messageOptional = Optional.empty();
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            if (!connection.isClosed()) {
                connection.rollback();
            }
            messageOptional = Optional.empty();
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        return messageOptional;
    }

    public List<Message> findFriendsMessages(User user) throws SQLException {
        List<Message> messages = new ArrayList<>();
        try (Neo4jPreparedStatement statement = new Neo4jPreparedStatement(connection, Message.FIND_FRIENDS_AND_MESSAGES_QUERY)) {
            statement.setLong(1, user.getNodeId());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                messages.add(new Message(rs.getLong(1), rs.getString(2), new User(rs.getLong(3), rs.getString(4), rs.getString(5), rs.getString(6)), new Date(rs.getLong(7)),
                        rs.getBoolean(8)));
            }
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
        return messages;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

}
