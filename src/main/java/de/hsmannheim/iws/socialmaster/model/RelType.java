package de.hsmannheim.iws.socialmaster.model;

public enum RelType {

    FRIEND_OF, POSTS, LIKES, MENTIONS;

    public static final String CREATE_FRIEND_OF_QUERY = String.format("MATCH (u:%s),(n:%s) WHERE id(u)={1} AND id(n)={2} MERGE (u)-[r:%s]->(n)",
            User.LABEL, User.LABEL,
            RelType.FRIEND_OF);
    public static final String CREATE_LIKES_QUERY = String.format("MATCH (u:%s),(m:%s) WHERE id(u)={1} AND id(m)={2} MERGE (u)-[r:%s]->(m)", User.LABEL, Message.LABEL,
            RelType.LIKES);

    public static final String DELETE_FRIEND_OF_QUERY = String.format("MATCH (u:%s)-[r:%s]-(m:%s) WHERE id(u)={1} AND id(m)={2} DELETE r", User.LABEL, RelType.FRIEND_OF,
            User.LABEL);
    public static final String DELETE_LIKES_QUERY = String.format("MATCH (u:%s)-[r:%s]-(m:%s) WHERE id(u)={1} AND id(m)={2} DELETE r", User.LABEL, RelType.LIKES, Message.LABEL);

}
