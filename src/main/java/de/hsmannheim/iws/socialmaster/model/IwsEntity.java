package de.hsmannheim.iws.socialmaster.model;

import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

public interface IwsEntity {

    public Map<String, Object> toMap();

    @XmlTransient
    public Long getNodeId();

}
