package de.hsmannheim.iws.socialmaster.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User implements IwsEntity, Serializable {
    private static final long serialVersionUID = 1169787158540702876L;
    public static final String LABEL = "USER";

    public static final String CREATE_QUERY = String.format("CREATE (u:%s {1}) RETURN id(u),u.firstname,u.lastname,u.email", LABEL);
    public static final String FIND_BY_MAIL = String.format("MATCH (u:%s) WHERE u.email={1} RETURN id(u),u.firstname,u.lastname,u.email", LABEL);
    public static final String FIND_ALL = String.format("MATCH (u:%s) RETURN id(u),u.firstname,u.lastname,u.email", LABEL);
    public static final String GET_ALL_FRIENDS_SUGGESTIONS = String.format(
            "MATCH (u:%s)-[:%s*2]-(w:%s) WHERE id(u)={1} AND NOT (u)-[:%s]-(w) RETURN DISTINCT id(w),w.firstname,w.lastname,w.email", User.LABEL, RelType.FRIEND_OF, User.LABEL,
            RelType.FRIEND_OF);
    public static final String GET_FRIENDS = String.format("MATCH (u:%s)-[:%s]-(x:%s) WHERE id(u) = {1} RETURN DISTINCT id(x),x.firstname,x.lastname,x.email", User.LABEL,
            RelType.FRIEND_OF, User.LABEL);

    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private transient String password;

    public User() {
    }

    public User(Long id, String firstname, String lastname, String email) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Map<String, Object> toMap() {
        Map<String, Object> objectAsMap = new HashMap<>();
        if (id != null && id > 0) {
            objectAsMap.put("id", this.id);
        }
        if (this.firstname != null) {
            objectAsMap.put("firstname", this.firstname);
        }
        if (this.lastname != null) {
            objectAsMap.put("lastname", this.lastname);
        }
        if (this.email != null) {
            objectAsMap.put("email", this.email);
        }
        return objectAsMap;
    }

    @Override
    public Long getNodeId() {
        return this.id;
    }

    @Override
    public String toString() {
        return String.format("[id=%s email=%s firstname=%s lastname=%s]", this.id, this.email, this.firstname, this.lastname);
    }

}
