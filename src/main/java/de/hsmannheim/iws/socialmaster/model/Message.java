package de.hsmannheim.iws.socialmaster.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Message implements IwsEntity, Serializable {

    private static final long serialVersionUID = -6831248078716607117L;
    public static final String LABEL = "MESSAGE";

    public static final String CREATE_QUERY = String.format(
            "MATCH (u:%s) WHERE id(u)={1} CREATE u-[p:%s {date:timestamp()}]->(m:%s {2}) RETURN id(m),m.content, p.date, id(u), u.firstname, u.lastname, u.email", User.LABEL,
            RelType.POSTS, Message.LABEL);
    public static final String FIND_BY_ID_QUERY = String.format("MATCH (u:%s) WHERE id(u)={1} RETURN id(u),u.content", LABEL);
    public static final String FIND_FRIENDS_AND_MESSAGES_QUERY = String.format(
            "MATCH (u:%s)-[r:%s*0..1]-(q:%s)-[p:%s]->(m:%s) WHERE id(u)={1} RETURN id(m),m.content,id(q),q.firstname, q.lastname, q.email, p.date, EXISTS((u)-[:%s]->(m)) AS liked_by_me ORDER BY p.date DESC",
            User.LABEL, RelType.FRIEND_OF, User.LABEL, RelType.POSTS, Message.LABEL, RelType.LIKES);

    private Long id;
    private String content;

    private Date date;

    private boolean likedByCurrentUser;

    private User sender;

    public Message() {
    }

    public Message(String content) {
        this.content = content;
    }

    public Message(Long id, String content) {
        this(content);
        this.id = id;
    }

    public Message(Long id, String content, User sender) {
        this.id = id;
        this.content = content;
        this.sender = sender;
    }

    public Message(Long id, String content, User sender, Date date, boolean likedByCurrentUser) {
        this(id, content, sender);
        this.date = date;
        this.likedByCurrentUser = likedByCurrentUser;
    }

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isLikedByCurrentUser() {
        return likedByCurrentUser;
    }

    public void setLikedByCurrentUser(boolean likedByCurrentUser) {
        this.likedByCurrentUser = likedByCurrentUser;
    }

    @Override
    public Map<String, Object> toMap() {
        Map<String, Object> objectAsMap = new HashMap<>();
        if (id != null && id > 0) {
            objectAsMap.put("id", this.id);
        }
        objectAsMap.put("content", this.content);
        if (sender != null) {
            objectAsMap.put("sender", sender.toMap());
        }
        return objectAsMap;
    }

    @Override
    public Long getNodeId() {
        return this.id;
    }

}
