package de.hsmannheim.iws.socialmaster.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Page<E> {

    private List<E> content;

    private int countPages;

    public Page(List<E> content, int countPages) {
        this.content = content;
        this.countPages = countPages;
    }

    @XmlElement
    public List<E> getContent() {
        return content;
    }

    @XmlElement
    public int getCountPages() {
        return countPages;
    }

}
