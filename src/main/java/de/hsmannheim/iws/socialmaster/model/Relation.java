package de.hsmannheim.iws.socialmaster.model;

import java.util.Date;

public class Relation {

    private final RelType relType;

    private Date creationDate;

    public Relation(RelType relType) {
        this.relType = relType;
    }

    public Relation(RelType relType, Date date) {
        this.relType = relType;
        this.creationDate = date;
    }

    public RelType getRelType() {
        return relType;
    }

    public Date getCreation() {
        return creationDate;
    }

    public void setCreation(Date creation) {
        this.creationDate = creation;
    }

}
