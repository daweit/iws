package de.hsmannheim.iws.socialmaster;

import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;

import org.neo4j.jdbc.Driver;
import org.neo4j.jdbc.Neo4jConnection;

@Stateful
public class Datasource {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    private Neo4jConnection connection;
    private String url = "//localhost:7474"; // <url>:<port>
    
    public Datasource() {
    }

    public Datasource(String url) {
        this.url = url;
    }

    @PostConstruct
    public void setup() {
        try {
            Class.forName("org.neo4j.jdbc.Driver");
            Properties neo4jProps = new Properties();
            neo4jProps.setProperty("user", "neo4j");
            neo4jProps.setProperty("password", "test");
            neo4jProps.setProperty("debug", "false");
            connection = new Driver().connect(String.format("jdbc:neo4j:%s", url), neo4jProps);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Unable to locate database on {0}", url);
        } catch (ClassNotFoundException c) {
            LOGGER.log(Level.WARNING, "Unable to find driver");
        }
    }

    @PreDestroy
    public void close() throws SQLException {
        if (!connection.isClosed()) {
            connection.close();
        }
    }

    public Neo4jConnection getConnection() {
        return connection;
    }
    
    public void setConnection(Neo4jConnection connection) {
        this.connection = connection;
    }


}
