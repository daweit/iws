package de.hsmannheim.iws.socialmaster;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/v1")
public class Configuration extends Application {
}
