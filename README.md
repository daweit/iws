Projekt um die Funktionsweise der Graphdatenbank Neoj4 im Rahmen des Informatik Workshops zu demonstrieren.


# Neo4j
Die Anwendung erwartet eine laufende Instanz an der Url "localhost:7474" mit den Zugangsdaten Benutzername:neo4j Passwort:test


# Authentifizierung
Als Authenzifizierungsverfahren kommt BASIC Auth zum Einsatz. Die Authentifizierung der Benutzer erfolgt über E-Mail und Passwort. Der Authorization Header muss daher mit Base64 codierter Email und Passwort versehen werden. 

# Services
Alle Services erwarten und akzeptieren ausschlißelich JSON bzw XML. Daher sind die Header "Content-Type" und "Accept" auf "application/json" zu setzen.

## User Services
* GET /v1/users - Liefert alle Benutzer (Kein Auth nötig)
* POST /v1/users  {"email":"foo@foo.de", "firstname":"foo", "lastname":"foo"} - Erstellt einen Benutzer (Kein Auth nötig)
* POST /v1/users/friend  {"email":"foo"} - Fügt den im Body angegebenen Benutzer als Freund hinzu (Auth nötig!)
* DELETE /v1/users/friend {"email":"foo"} - Beendet die Freundschaft mit den im Body angegebenen Benutzer (Auth nötig!)
* GET /v1/users/friendsuggestions	- Liefert alle Freunde 2. Grades (Auth nötig!)
* GET /v1/users/friends - Liefert alle Freunde (Auth nötig!)

## Message Services
* POST /v1/messages {"content":"lorem ipsum"} - Erstellt eine Nachricht (Auth nötig!)
* POST /v1/messages/like {"id":42}	- Erstellt ein Like auf die angegebene Nachricht (Auth nötig!)
* GET /v1/messages?hashtags=yolo&hashtags=zuvielzeit - Liefert alle Nachrichten von Freunden (Auth nötig! - Achtung: Hashtagfilter nur Dummy)
* GET /v1/messages/like - Liefert alle gelikten Nachrichten von Freunden (Auth nötig! - Achtung: nur Dummy)

## Hashtag Services (Zusatzaufgabe)
* GET /v1/hashtags - Liefert alle vorhandenen Hashtags (Auth nötig! - Achtung: Dummy Impl)
